(ns ^:figwheel-always minimal-om-bug.core
    (:require
     [om.core :as om :include-macros true]
     [om-tools.dom :as dom :include-macros true]))

(enable-console-print!)

(println "Edits to this text should show up in your developer console.")

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (atom {:bars [{:blocks []}
                                 {:blocks [5 4 3 2 1]}
                                 {:blocks [3 2 1]}]
                          :text "Hello world!"}))
(defn consv [el v]
  (vec (cons el v)))
(defn add-block [bar owner]
  (reify
    om/IRender
    (render [this]
      (dom/button {:style {:display "inline"
                           :margin "15px"}
                   :on-click
                   (fn [e]
                     (om/transact!
                      bar :blocks
                      (fn [blocks]
                        (consv (inc (or (first blocks) 0)) blocks))))}
        "Add Block"))))

(defn block [n owner]
  (reify
    om/IRender
    (render [this]
      (dom/div {:style {:margin "3px" :background-color "#8a8"
                        :display "inline"}}
        n))))

(defn bar [{:keys [bar]} app]
  (reify
    om/IRender
    (render [this]
      (dom/div {:style
                {:display "block" :margin "15px" :background-color "#a88"} }
        (om/build-all
         block
         (:blocks bar))
        (om/build add-block bar)))))

(defn add-bar [app owner]
  (reify
    om/IRender
    (render [this]
      (dom/button {:on-click
                   (fn [e]
                     (om/transact!
                      app :bars
                      (fn [bars]
                        (consv {:blocks []} bars))))}
        "Add Bar"))))

(defn root-view [app owner]
  (reify
    om/IRender
    (render [this]
      (dom/div {}
        (om/build-all bar (map (fn [b] {:bar b}) (:bars app)))
        (om/build add-bar app)))))



;;;;;;;;;;;;;;;;;;;;;;;;;; Initialize Things ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn main []
  (om/root
   root-view
   app-state
   {:target (. js/document (getElementById "app"))}))

(main)
